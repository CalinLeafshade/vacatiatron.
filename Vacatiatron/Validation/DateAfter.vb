﻿Imports System.ComponentModel.DataAnnotations

Public Class DateAfterOrOn
    Inherits ValidationAttribute
    Implements IClientValidatable

    Private prop As String

    Public Sub New(propName As String)
        prop = propName
    End Sub

    Public Sub New(propName As String, err As String)
        MyBase.New(err)
        prop = propName
    End Sub

    Protected Overrides Function IsValid(value As Object, validationContext As ValidationContext) As ValidationResult

        Dim propertyTestedInfo = validationContext.ObjectType.GetProperty(prop)

        If propertyTestedInfo Is Nothing Then
            Return New ValidationResult(String.Format("unknown property {0}", prop))
        End If

        Dim propertyTestedValue = propertyTestedInfo.GetValue(validationContext.ObjectInstance, Nothing)

        If value Is Nothing Or Not TypeOf value Is Date Then
            Return ValidationResult.Success
        End If

        If propertyTestedValue Is Nothing Or Not (TypeOf propertyTestedValue Is Date) Then
            Return ValidationResult.Success
        End If

        If CType(value, Date) >= CType(propertyTestedValue, Date) Then
            Return ValidationResult.Success
        End If

        Return New ValidationResult(FormatErrorMessage(validationContext.DisplayName))

    End Function

    Public Function GetClientValidationRules(metadata As ModelMetadata, context As ControllerContext) As IEnumerable(Of ModelClientValidationRule) Implements IClientValidatable.GetClientValidationRules
        Dim rule = New ModelClientValidationRule() With
        {
            .ErrorMessage = Me.ErrorMessageString,
            .ValidationType = "isdateafter"
        }
        rule.ValidationParameters("propertytested") = Me.prop
        Return {rule}
    End Function
End Class
