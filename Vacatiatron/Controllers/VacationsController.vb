﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.Entity
Imports System.Linq
Imports System.Threading.Tasks
Imports System.Net
Imports System.Web
Imports System.Web.Mvc
Imports Vacatiatron
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports System.Net.Mail
Imports System.Threading

Namespace Controllers
    <Authorize>
    Public Class VacationsController
        Inherits System.Web.Mvc.Controller

        ' NB: Used VbCrLF and not Environment.NewLine because it's a Const expression
        Private Const STATUSCHANGEDEMAIL As String = "Dear {0}," & vbCrLf &
                                                    vbCrLf &
                                                    "The status of your vacation request has been modified. It is now in {1} status." & vbCrLf &
                                                    vbCrLf &
                                                    "Sincerely, The Management"

        Private Const NEWVACATIONREQUEST As String = "Dear Admin" & vbCrLf &
                                                    vbCrLf &
                                                    "A new vacation request has been submitted."

        Private db As New ApplicationDbContext()
        Private userManager As New UserManager(Of ApplicationUser)(New UserStore(Of ApplicationUser)(db))

        Async Function Dashboard() As Task(Of ActionResult)
            Return RedirectToAction("Index") 'TODO add dashaboard
        End Function

        ' GET: Vacations
        Async Function Index() As Task(Of ActionResult)

            Dim currentUser = Await userManager.FindByIdAsync(User.Identity.GetUserId())
            If currentUser Is Nothing Then ' This wouldnt normally happen but it might occur if a user were authed by google but had no DB record (like if the app had been restarted and the DB dropped)
                Return RedirectToAction("Index", "Home")
            End If
            Dim usersVacations = From va In db.Vacations Order By va.Status, va.StartDate Where va.Requester.Id = currentUser.Id
            If usersVacations Is Nothing Then
                Return View()
            Else
                Return View(Await usersVacations.ToListAsync())
            End If
        End Function

        ' GET: Vacations/Details/5
        Async Function Details(ByVal id As Integer?) As Task(Of ActionResult)
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim vacation As Vacation = Await db.Vacations.FindAsync(id)
            If IsNothing(vacation) Then
                Return HttpNotFound()
            End If
            Return View(vacation)
        End Function

        <Authorize(Roles:="Admin")>
        Async Function Admin() As Task(Of ActionResult)
            Dim vacs = From va In db.Vacations Order By va.Status, va.StartDate
            Dim list = Await vacs.ToListAsync()
            Dim viewList = New List(Of VacationViewModel)()
            For Each v In list
                Dim vm = New VacationViewModel(v)
                Dim conflicts = Await (From va In db.Vacations
                                Where va.ID <> v.ID And Not (va.Requester Is Nothing)
                                Select va).ToListAsync()

                For Each conflictingVa In conflicts.Where(Function(x) x.ConflictsWith(v))
                    vm.Notes.Add(String.Format("Conflict with vacation for {0}", conflictingVa.Requester.Email))
                Next
                viewList.Add(vm)
            Next v
            Return View(viewList)
        End Function

        ' GET: Vacations/Create
        Function Create() As ActionResult
            Return View()
        End Function

        ' POST: Vacations/Create
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Async Function Create(<Bind(Include:="ID,StartDate,EndDate")> ByVal vacation As Vacation) As Task(Of ActionResult)
            If ModelState.IsValid Then
                Dim id = User.Identity.GetUserId()
                Dim currentUser = Await userManager.FindByIdAsync(id) ' Be careful when restarting the app. The auth cookie might stil be present but the db record will not, causing an error.
                vacation.Requester = currentUser
                Console.WriteLine(currentUser.Id)
                db.Vacations.Add(vacation)
                Await db.SaveChangesAsync()
                Await EmailAdmin(NEWVACATIONREQUEST)
                Return RedirectToAction("Index")
            End If
            Return View(vacation)
        End Function

        ' GET: Vacations/Edit/5
        Async Function Edit(ByVal id As Integer?) As Task(Of ActionResult)
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim vacation As Vacation = Await db.Vacations.FindAsync(id)
            If IsNothing(vacation) Then
                Return HttpNotFound()
            End If
            Return View(vacation)
        End Function

        ' POST: Vacations/Edit/5
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Async Function Edit(<Bind(Include:="ID,StartDate,EndDate,Status")> ByVal vacation As Vacation) As Task(Of ActionResult)
            If ModelState.IsValid Then
                db.Entry(vacation).State = EntityState.Modified
                Await db.SaveChangesAsync()
                Return RedirectToAction("Index")
            End If
            Return View(vacation)
        End Function

        Private Sub EmailUser(ByRef user As ApplicationUser, ByVal message As String)
            'Obviously a lot of this would be stored in a cfg somewhere. This is just for show.
            Dim mail As New MailMessage()
            mail.From = New MailAddress("ouradmin@googlemail.com")
            mail.To.Add(user.Email)
            mail.IsBodyHtml = False
            mail.Body = message
            Dim smtp As New SmtpClient("smtp.gmail.com", 587)
            ' Obviously needs credential here. But it's tested as working
            smtp.Credentials = New Net.NetworkCredential("XXXX", "XXXX")
            smtp.EnableSsl = True

            'This should be done in a task table or something rather than with an adhoc thread worker.
            ' pass empty object, would add completion logic later
            ThreadPool.QueueUserWorkItem(Sub(o) smtp.SendAsync(mail, o), New Object)

        End Sub

        Private Async Function EmailAdmin(ByVal message As String) As Task
            ' This bit is stupid. I'd cache this in a real app and store it elsewhere probably but it works here.
            Dim query = From u In db.Users Select u

            'Need to force the linq down to object level to release the data reader.
            For Each us In Await query.ToListAsync()
                If Await userManager.IsInRoleAsync(us.Id, "Admin") Then
                    EmailUser(us, message)
                End If
            Next
            ' Failed to find admin
        End Function

        <Authorize(Roles:="Admin")>
        Async Function ChangeStatus(ByVal id As Integer?, ByVal status As Vacation.RequestState?) As Task(Of ActionResult)
            If IsNothing(id) Or IsNothing(status) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim vacation As Vacation = Await db.Vacations.FindAsync(id)
            If IsNothing(vacation) Then
                Return HttpNotFound()
            Else
                If vacation.Status <> status Then
                    vacation.Status = status
                    Await db.SaveChangesAsync()
                    EmailUser(vacation.Requester, String.Format(STATUSCHANGEDEMAIL, If(vacation.Requester.UserName, vacation.Requester.Email), vacation.Status.ToString()))
                End If
            End If
            Return RedirectToAction("Admin")
        End Function

        ' GET: Vacations/Delete/5
        Async Function Delete(ByVal id As Integer?) As Task(Of ActionResult)
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim vacation As Vacation = Await db.Vacations.FindAsync(id)
            If IsNothing(vacation) Then
                Return HttpNotFound()
            End If
            Return View(vacation)
        End Function

        ' POST: Vacations/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        Async Function DeleteConfirmed(ByVal id As Integer) As Task(Of ActionResult)
            Dim vacation As Vacation = Await db.Vacations.FindAsync(id)
            db.Vacations.Remove(vacation)
            Await db.SaveChangesAsync()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace
