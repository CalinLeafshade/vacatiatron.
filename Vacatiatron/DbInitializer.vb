﻿Imports System.Data.Entity
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework

Public Class DbInitializer
    Inherits DropCreateDatabaseAlways(Of ApplicationDbContext)
    Protected Overrides Sub Seed(context As ApplicationDbContext)

        Dim UserManager = New UserManager(Of ApplicationUser)(New UserStore(Of ApplicationUser)(context))

        Dim RoleManager = New RoleManager(Of IdentityRole)(New RoleStore(Of IdentityRole)(context))

        If Not RoleManager.RoleExists("Admin") Then
            RoleManager.Create(New IdentityRole("Admin"))
        End If

        MyBase.Seed(context)
    End Sub
End Class
