﻿@ModelType IEnumerable(Of Vacatiatron.VacationViewModel)
@Code
ViewData("Title") = "Admin"
End Code

<h2>Admin</h2>

<table class="table">
    <tr>
        <th>
            Requester Email
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.StartDate)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.EndDate)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Status)
        </th>
        <th>
            Notes
        </th>
        <th>Actions</th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Requester.Email)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.StartDate)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.EndDate)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Status)
        </td>
        <td>
            <ul>
                @For Each note In item.Notes
                    @:<li>
                        @note
                    @:</li>
                Next
            </ul>
        </td>
        <td>
            @Html.ActionLink("Approve", "ChangeStatus", New With {.id = item.Vacation.ID, .status = Vacation.RequestState.Approved}) |
            @Html.ActionLink("Conflict", "ChangeStatus", New With {.id = item.Vacation.ID, .status = Vacation.RequestState.Conflict}) |
            @Html.ActionLink("Deny", "ChangeStatus", New With {.id = item.Vacation.ID, .status = Vacation.RequestState.Denied}) |
        </td>
    </tr>
Next

</table>
