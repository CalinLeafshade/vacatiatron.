﻿@Code
    ViewData("Title") = "Index"
End Code

<div class="jumbotron">
    <h1>ASP.NET</h1>
    <p class="lead">The vacatiatron is an ASP app that allows users to make requests for holidays to their management.</p>
</div>

<div class="row">
    <div class="col-md-4">
        <h2>Custom validation</h2>
        <p>
            The vacatiatron includes custom model validation classes like DateAfterOrOn which uses reflection to compare two dates in the same model.
        </p>
    </div>
    <div class="col-md-4">
        <h2>LINQ Queries</h2>
        <p>The Vacatiatron uses LINQ to asynchronously query the DB</p>
    </div>
    <div class="col-md-4">
        <h2>Further stuff</h2>
        <p>Stuff.</p>
    </div>
</div>
