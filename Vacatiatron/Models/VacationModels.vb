﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity
Imports Microsoft.AspNet.Identity.EntityFramework


Public Class Vacation

    Public Enum RequestState
        Pending
        Conflict
        Denied
        Approved
    End Enum

    <Key>
    Public Property ID As Integer
    <Required>
    <DataType(DataType.Date)>
    <Display(Name:="Start Date")>
    Public Property StartDate As Date
    <Required>
    <DataType(DataType.Date)>
    <Display(Name:="End Date")>
    <DateAfterOrOn("StartDate", "The end date must fall on or after the start date")>
    Public Property EndDate As Date
    Public Overridable Property Requester As ApplicationUser

    Public Property Status As RequestState

    Public Sub New()
        Status = RequestState.Pending
    End Sub

    Public Function ConflictsWith(other As Vacation)
        Return Date.Compare(Me.StartDate, other.EndDate) <= 0 And Date.Compare(Me.EndDate, other.StartDate) >= 0
    End Function

End Class

Public Class VacationViewModel
    Public Property Vacation As Vacation
    Public Property Notes As List(Of String) ' Should probably encapsulate this more thoroughly but for a view model it's probably ok

    ' Access props for ease
    <DataType(DataType.Date)>
    <Display(Name:="Start Date")>
    Public Property StartDate As Date
        Get
            Return Vacation.StartDate
        End Get
        Set(value As Date)
            Vacation.StartDate = value
        End Set
    End Property

    <DataType(DataType.Date)>
    <Display(Name:="End Date")>
    Public Property EndDate As Date
        Get
            Return Vacation.EndDate
        End Get
        Set(value As Date)
            Vacation.EndDate = value
        End Set
    End Property

    Public Property Status As Vacation.RequestState
        Get
            Return Vacation.Status
        End Get
        Set(value As Vacation.RequestState)
            Vacation.Status = value
        End Set
    End Property

    Public Property Requester As ApplicationUser
        Get
            Return Vacation.Requester
        End Get
        Set(value As ApplicationUser)
            Vacation.Requester = value
        End Set
    End Property

    Sub New(va As Vacation)
        If va Is Nothing Then
            Throw New ArgumentNullException("Vacation cannot be null")
        End If
        Me.Vacation = va
        Me.Notes = New List(Of String)
    End Sub
End Class

